# VIDEO FUTUR PROGRAM

 - Link to the GitLab project: https://gitlab.com/jerome726/videofutur.git
 - Java version: 21

# About the Program
This simple java program allows users to choose movies (see list below) and save them in a cart. At any time, user can check the total price of the carts some movies may have promotions with different rules.

Run this program: 
 - **with source code**: You can use Visual Studio Code
 - **with jar**: in videofutur directory, go in `target`, open your favourite order prompt then `java -jar videofutur-1.0.0-jar-with-dependencies.jar`

Two files containing data for the project are used:
 - `movies.json`: contains all available films
 - `promo.json`: contains all current promotions

Two types of promotions:
 - Fixed price
 - Percentage off the total for a certain quantity purchased (using the same discount)

Three discount coupons are handled in `promo.json`:
 - `BUYONE2024`: Fixed price
 - `BUYTWO2024`: Percentage off for purchasing two items
 - `BUYTHREE2024`: Percentage off for purchasing three items

List of available movies in `movies.json`:
 - back to the future
 - back to the future 2
 - back to the future 3
 - e.t.
 - raiders of the lost ark
 - ghostbusters
 - the empire strikes back
 - the shining
 - blade runner
 - la chevre
 - rambo
 - top gun
 - terminator
 - beetlejuice
 - die hard
 - the goonies
  
Note: Case sensitivity must be respected.

# Commands:
There commands are available for users:
 - `EXIT`: Ends the program
 - `RESET`: Resets the cart
 - `TOTAL`: Displays the current total, whenever you want

# utiles:
As a base project for a future team, several elements have been added to initiate the work of developers:
 - A project on GitLab
 - A YAML file to configure a GitLab pipeline
 - An initial unit test file
 - A constants file
 - A properties file
  
# TODO
Future developers can evolve this program by:
 - Allowing case-insensitive movie input
 - Implementing an autocomplete or fuzzy search system
 - remove movies from cart (you can only add)
 - Adding a test file for each class and creating scenarios
 - replace json data file by a real data base (like mongo, couch, etc)
 - add values into `config.properties`, this file can be used with PropertiesUtil.java
 - etc.

# Examples
Here are a few examples of use:

Example 1
Input:
 - Back to the future 1
 - Back to the future 2
 - Back to the future 3
Output: 36

Example 2
Input:
 - Back to the future 1
 - Back to the future 3
Output: 27

Example 3:
Input:
 - Back to the future 1
Output: 15

Example 4:
Input:
 - Back to the future 1
 - Back to the future 2
 - Back to the future 3
 - Back to the future 2
Exit: 48

Example 5
Input:
 - Back to the future 1
 - Back to the future 2
 - Back to the future 3
 - la chevre
Exit: 56



