package videofutur.command;

/**
 * All comands that user can use.
 */
public class CommandEnum {

  public static final String EXIT = "exit";
  public static final String RESET = "reset";
  public static final String TOTAL = "total";
}
