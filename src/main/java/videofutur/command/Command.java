package videofutur.command;

import java.util.Map;
import java.util.Scanner;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import videofutur.Constants;
import videofutur.cart.Cart;
import videofutur.cart.PromoManager;
import videofutur.utils.FileUtil;

/**
 * Main class to interpret suer command.
 */
public class Command {

  private static final Command instance = new Command();
  private FileUtil fileManager = new FileUtil();
  private Cart cart = new Cart();
  private PromoManager promoManager = new PromoManager();
  private Map<String, JSONObject> movieList;

  /**
   * Constructor
   */
  public Command() {
    this.fileManager.openData(Constants.PATH_MOVIES);
    this.movieList = this.fileManager.readData();
  }

  /**
   * Singleton
   */
  public static final Command getInstance() {
    return instance;
  }

  public void doLoopCommand() {
    try (Scanner scanner = new Scanner(System.in)) {
      while (true) {
        System.out.print("Saisir un film : ");
        String line = scanner.nextLine();

        // Stop app
        if (line.equalsIgnoreCase(CommandEnum.EXIT)) {
          break;
        }
        // Reset cart
        if (line.equalsIgnoreCase(CommandEnum.RESET)) {
          this.resetCommand();
          // Screen total
        } else if (line.equalsIgnoreCase(CommandEnum.TOTAL)) {
          this.resumeCart();
        } else {
          this.analyseMovie(line);
        }
      }
      System.out.println("fin");
    } catch (Exception e) {
      System.out.print(e.getMessage());
    }
  }

  private void analyseMovie(String name) {
    // Error
    if (name == null || name.equals("")) {
      System.out.println("erreur: valeur vide");
    } else {
      // Ok, check this movie
      JSONObject value = this.movieList.get(name);
      if (value != null) {
        // Get data from found movie
        double price = ((Number) value.get("price")).doubleValue();
        // Is there a "promo" ?
        JSONArray promoArray = (JSONArray) value.get("promo");
        if (promoArray.size() > 0) {
          double newTotal =
            this.getNewTotalWithPromo(
                promoArray,
                price,
                this.cart.ifMovieAlreadyInCart(name)
              );
          this.cart.addMovie(name, newTotal);
        } else {
          // No promo
          this.promoManager.addRegularPrice(price);
          double newTotal = price + this.cart.getTotal();
          this.cart.addMovie(name, newTotal);
        }
      } else {
        // Not found
        System.out.println("\"".concat(name).concat("\" non trouvé"));
      }
    }
  }

  private double getNewTotalWithPromo(
    JSONArray promos,
    double price,
    boolean alreadyInCart
  ) {
    double total = this.cart.getTotal();
    return this.promoManager.getPromo(promos, price, total, alreadyInCart);
  }

  private void resumeCart() {
    double total = this.cart.getTotal();
    System.out.println(
      "Total de votre panier: ".concat(String.valueOf(total)).concat(" euros")
    );
  }

  private void resetCommand() {
    this.cart.resetCart();
    this.promoManager.resetPromotion();
    System.out.println("Remise à zéro du panier ok");
  }
}
