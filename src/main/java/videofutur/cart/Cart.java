package videofutur.cart;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages the total price calculation for movies.
 */
public class Cart {

  // Array to store movie names
  private List<String> movieNames;
  // Total price
  private double total;

  /**
   * Constructor
   */
  public Cart() {
    this.movieNames = new ArrayList<>();
    this.total = 0;
  }

  /**
   * Getter on movieNames
   * @return movieNames list
   */
  public List<String> getCart() {
    return this.movieNames;
  }

  /**
   * Getter on moviesPrices size
   * @return int size
   */
  public int getNumberOfMovies() {
    return this.movieNames.size();
  }

  /**
   * Adds a movie with its price to the map.
   * @param {String} movieName The name of the movie.
   * @param {double} newTotal The total cart.
   */
  public void addMovie(String movieName, double newTotal) {
    this.movieNames.add(movieName);
    this.total = newTotal;
  }

  /**
   * Calculates the total price of all movies.
   * @return The total price.
   */
  public double getTotal() {
    return this.total;
  }

  /**
   * Resets the cart (list name and total).
   */
  public void resetCart() {
    this.movieNames.clear();
    this.total = 0;
  }

  /**
   * Checks if a movie already exists in the movieNames list.
   * @param {String} movieName The name of the movie to check.
   * @return true if the movie already exists in the list, otherwise false.
   */
  public boolean ifMovieAlreadyInCart(String movie) {
    return movieNames.contains(movie);
  }
}
