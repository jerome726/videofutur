package videofutur.cart;

/**
 * All kind of promos
 */
public class PromoEnum {

  public static final String NEW_PRICE = "new_price";
  public static final String DECREASING_QUANTITY = "decreasing_quantity";
}
