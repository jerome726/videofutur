package videofutur.cart;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import videofutur.Constants;
import videofutur.utils.FileUtil;

/**
 * Manage promos and all their rules
 */
public class PromoManager {

  private Map<String, JSONObject> promoList;
  private FileUtil fileManager = new FileUtil();
  // Array to store current promo prices to compute the final total with target rate
  private List<Double> promoPrices;
  // Store regular prices in case of promo to compute all prices
  private List<Double> regularPrices;
  private double rate = 0.0;
  private int eligibleQuantity = 0;

  /**
   * Constructor
   */
  public PromoManager() {
    this.fileManager.openData(Constants.PATH_PROMO);
    this.promoList = this.fileManager.readData();
    this.promoPrices = new ArrayList<>();
    this.regularPrices = new ArrayList<>();
  }

  public void addRegularPrice(double price) {
    this.regularPrices.add(price);
  }

  /**
   * Main function of the class: compute promotion
   * @param {JSONArray} promos JSONArray containing promotions
   * @param {double} price original price of the item
   * @param {double} total current total price
   * @param {boolean} alreadyInCart flag indicating whether the item is already in the cart
   * @return the price after applying promotions
   */
  public double getPromo(
    JSONArray promos,
    double price,
    double total,
    boolean alreadyInCart
  ) {
    // 1- new_price
    for (Object promo : promos) {
      JSONObject value = this.promoList.get(promo);
      if (value.get("type").equals(PromoEnum.NEW_PRICE)) {
        price = ((Long) value.get("price")).intValue();
      }
    }

    // 2- If promos contains a decreasing quantity, then store it to compute it now or later
    this.promoIsDecreasingQuantity(promos, price, alreadyInCart);

    // 3- Compute decreasing_quantity
    for (Object promo : promos) {
      JSONObject value = this.promoList.get(promo);

      // When promo quantity match with current quantity
      int quantity = ((Long) value.get("quantity")).intValue();
      if (
        value.get("type").equals(PromoEnum.DECREASING_QUANTITY) &&
        this.eligibleQuantity == quantity
      ) {
        this.rate = ((Number) value.get("rate")).doubleValue();
      }
    }

    // If eligible prices for the promotion are available
    if (this.promoPrices.size() > 0 && this.rate > 0) {
      double sum = this.promoPrices.stream().reduce(0.0, Double::sum);
      double totalPromo = (sum * (1 - (this.rate / 100)));
      double sumOtherPrices =
        this.regularPrices.stream().reduce(0.0, Double::sum);
      return totalPromo + sumOtherPrices;
    }

    return price + total;
  }

  /**
   * Store new price if one promo is a decreasing quantity to compute total with rate
   * @param {JSONArray} promos JSONArray containing promotions
   * @param {double} price
   */
  private void promoIsDecreasingQuantity(
    JSONArray promos,
    double price,
    boolean alreadyInCart
  ) {
    for (Object promo : promos) {
      JSONObject value = this.promoList.get(promo);
      if (value.get("type").equals(PromoEnum.DECREASING_QUANTITY)) {
        // If movie has been already put in cart
        if (!alreadyInCart) {
          this.eligibleQuantity++;
        }
        promoPrices.add(price);
        break;
      }
    }
  }

  /**
   * Resets all promotion values).
   */
  public void resetPromotion() {
    this.rate = 0.0;
    this.eligibleQuantity = 0;
    this.promoPrices.clear();
    this.regularPrices.clear();
  }
}
