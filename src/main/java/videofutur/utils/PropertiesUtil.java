package videofutur.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import videofutur.Constants;

/*
 * For readind properties file
 */
public class PropertiesUtil {

  private Properties properties;

  /**
   * Constructor
   */
  public PropertiesUtil() {
    InputStream inputStream = getClass()
      .getClassLoader()
      .getResourceAsStream(Constants.PATH_PROP);
    loadProperties(inputStream);
  }

  /**
   * Load properties file
   * @param {String} path
   */
  private void loadProperties(InputStream inputStream) {
    try (
      BufferedReader reader = new BufferedReader(
        new InputStreamReader(inputStream)
      )
    ) {
      properties = new Properties();
      properties.load(reader);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Getter on propertie
   * @param {String} key
   * @return the propertie
   */
  public String getProperty(String key) {
    return properties.getProperty(key);
  }
}
