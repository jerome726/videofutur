package videofutur.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * This class manage all json file : open, read and store data.
 */
public class FileUtil {

  // Current data from file, store in memory
  private JSONArray jsonArray;

  /**
   * Open the file with his path.
   * @param {String} path.
   */
  public void openData(String path) {
    JSONParser parser = new JSONParser();
    try {
      InputStream inputStream = getClass().getResourceAsStream(path);
      if (inputStream != null) {
        Object obj = parser.parse(new InputStreamReader(inputStream));
        this.jsonArray = (JSONArray) obj;
      } else {
        throw new IOException("File not found: " + path);
      }
    } catch (IOException | ParseException e) {
      e.printStackTrace();
    }
  }

  /**
   * Put all data from json file into a Map.
   * @return {<String, JSONObject>} the Map.
   */
  public Map<String, JSONObject> readData() {
    Map<String, JSONObject> resultMap = new HashMap<>();
    for (Object o : this.jsonArray) {
      JSONObject jsonObject = (JSONObject) o;
      String key = (String) jsonObject.get("name");
      resultMap.put(key, jsonObject);
    }
    return resultMap;
  }
}
