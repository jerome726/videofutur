package videofutur;

import videofutur.command.Command;

/**
 * Main
 */
public class App {

  public static void main(String[] args) {
    Command command = Command.getInstance();
    command.doLoopCommand();
  }
}
