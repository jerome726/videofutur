package videofutur;

/**
 * All Constants
 */
public class Constants {

  public static final String PATH_PROP = "config.properties";
  public static final String PATH_PROMO = "/data/promo.json";
  public static final String PATH_MOVIES = "/data/movies.json";
}
