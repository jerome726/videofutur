package videofutur;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import videofutur.cart.Cart;

/**
 * Unit test for simple App.
 */
public class CartManagerTest {

  /**
   * Test CartManager class
   */
  @Test
  public void shouldAnswerWithTrue() {
    Cart cart = new Cart();
    cart.addMovie("Movie 1", 20);
    cart.addMovie("Movie 2", 45);

    // Check getCart
    List<String> cartList = cart.getCart();
    // Create a reference list for comparison
    List<String> expectedCartList = new ArrayList<>();
    expectedCartList.add("Movie 1");
    expectedCartList.add("Movie 2");
    // Verify that both lists are equal
    assertEquals(expectedCartList, cartList);

    // Checks if a movie already exist
    boolean isInCart = cart.ifMovieAlreadyInCart("Movie 1");
    assertEquals(isInCart, true);
    boolean isNotInCart = cart.ifMovieAlreadyInCart("Movie 3");
    assertEquals(isNotInCart, false);

    // Total cart
    double total = cart.getTotal();
    assertEquals(45.0, total, 0.001);

    // Number of movies into cart
    int nbr = cart.getNumberOfMovies();
    assertEquals(nbr, 2);

    // Test Reset cart
    cart.resetCart();
    // Then test again the smae functions
    double totalReset = cart.getTotal();
    assertEquals(0.0, totalReset, 0.001);
    int nbrReset = cart.getNumberOfMovies();
    assertEquals(nbrReset, 0);
  }
}
